import { gql } from "@apollo/client";

export const GET_CUPS = gql`
  query {
    cups {
      id
      name
      type
      caffeine
    }
  }
`;
