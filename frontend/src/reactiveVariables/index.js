import { makeVar } from "@apollo/client";

// const defaultFilters = {
//   name: "string",
//   type: "string",
//   caffeine: "boolean",
// };

const defaultFilters = {
  name: {
    type: "string",
    value: "",
    available: ["string"],
  },
  //   type: {
  //     type: "string",
  //     value: "",
  //   },
  type: {
    type: "select",
    value: "",
    available: ["string", "select"],
  },
  caffeine: {
    type: "range",
    value: [0, 20],
    available: ["boolean", "range"],
  },
};

export const filters = makeVar(defaultFilters);
export const cups = makeVar([]);

// Saving a list
// - server doesn't know custom FE filters
// - save a list of ids? - BE data changes -> FE will be stale
