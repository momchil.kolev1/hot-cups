import { createMuiTheme } from "@material-ui/core/styles";

// #244C60 // navy blue
// #970C10 // red
// #232425 // black

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#EFF4F0", // cream
    },
    secondary: {
      main: "#384640", // charcoal
    },
    blue: {
      main: "#244C60",
    },
    red: {
      main: "#970C10",
    },
    black: {
      main: "#232425",
    },
  },
  typography: {
    fontFamily: ["Dancing Script", "Montserrat", "sans serif"].join(","),
    body1: {
      fontSize: "1rem",
      fontFamily: ["Montserrat", "sans serif"].join(","),
    },
    body2: {
      fontSize: "0.875rem",
      fontFamily: ["Montserrat", "sans serif"].join(","),
    },
  },
});

export default theme;
