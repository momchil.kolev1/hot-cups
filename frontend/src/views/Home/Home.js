import React from "react";
import { makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

const useStyles = makeStyles({
  root: {
    marginTop: "100px",
  },
  icon: {
    fontSize: "96px",
    textDecoration: "none",
  },
});

export default function Home() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography variant="h1">
        Tea or Coffee?
        <br />
        Browse and Compare
      </Typography>
      <Link to="/browse">
        <ChevronRightIcon
          classes={{
            root: classes.icon,
          }}
          color="primary"
        />
      </Link>
    </div>
  );
}
