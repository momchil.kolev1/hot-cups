import React from "react";
import { makeStyles } from "@material-ui/core";
import { useQuery, useReactiveVar } from "@apollo/client";

import { GET_CUPS } from "../../queries/index";
import Filters from "./components/Filters/Filters";
import Cup from "./components/Cup/Cup";
import {
  filters as filtersVar,
  cups as cupsVar,
} from "../../reactiveVariables/index";
import { filter, snakeCase } from "lodash";

const useStyles = makeStyles({
  root: {
    display: "grid",
    gridTemplateColumns: "300px 1fr",
    gridGap: "50px",
    padding: "50px",
  },
  cups: {
    display: "grid",
    gridTemplateColumns: "repeat(auto-fit, minmax(160px, 1fr))",
    gridGap: "20px",
  },
});

export default function Browse() {
  const classes = useStyles();
  const filters = useReactiveVar(filtersVar);
  const { data, loading, error } = useQuery(GET_CUPS);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error}</p>;
  const { cups } = data;
  cupsVar(cups);

  const filterCups = (c) => {
    if (filters.name.value && typeof filters.name.value === "string")
      if (!c.name.toLowerCase().includes(filters.name.value.toLowerCase()))
        return false;
    if (filters.type.value)
      if (!snakeCase(c.type).includes(snakeCase(filters.type.value)))
        return false;
    if (Array.isArray(filters.caffeine.value)) {
      if (
        c.caffeine < filters.caffeine.value[0] ||
        c.caffeine > filters.caffeine.value[1]
      )
        return false;
    } else if (
      (filters.caffeine.value === true && c.caffeine === 0) ||
      (filters.caffeine.value === false && c.caffeine > 0)
    )
      return false;
    return c;
  };
  /*
  {
    id: Int,
    name: String,
    type: String/Select,
    caffeine: Int/Range/Boolean
  }
  */

  return (
    <div className={classes.root}>
      <div>
        <Filters />
      </div>
      <div className={classes.cups}>
        {cups.filter(filterCups).map((c) => (
          <Cup {...c} key={c.id} />
        ))}
      </div>
    </div>
  );
}
