import React from "react";
import { makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { useReactiveVar } from "@apollo/client";

import { filters as filtersVar } from "../../../../reactiveVariables/index";
import Filter from "./components/Filter/Filter";
import { ReactComponent as ManageIcon } from "../../../../icons/Subtract.svg";

const useStyles = makeStyles({
  root: {
    display: "grid",
    justifyContent: "center",
    gridGap: "40px",
  },
  filter: {
    display: "grid",
    gridTemplateColumns: "min-content auto min-content",
    justifyContent: "space-between",
    gridGap: "20px",
    textTransform: "capitalize",
    "& > *:nth-child(2)": {
      gridColumn: "3/4",
      cursor: "pointer",
    },
    "& > *:last-child": {
      gridColumn: "1/-1",
    },
  },
});

export default function Filters() {
  const classes = useStyles();
  const filters = useReactiveVar(filtersVar);
  console.log("rerender Filters");

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [currentFilter, setCurrentFilter] = React.useState("");
  const [filterState, setFilterState] = React.useState({});

  const handleClick = (filter) => (event) => {
    setCurrentFilter(filter);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (e) => {
    const tmp = filters;
    // Save filter state before switching
    setFilterState({
      [currentFilter]: tmp[currentFilter],
    });
    if (e.target.textContent.toLowerCase() === "range") {
      filtersVar({
        ...tmp,
        [currentFilter]: {
          ...tmp[currentFilter],
          type: e.target.textContent.toLowerCase(),
          value: [0, 20],
        },
      });
    } else
      filtersVar({
        ...tmp,
        [currentFilter]: {
          ...tmp[currentFilter],
          type: e.target.textContent.toLowerCase(),
        },
      });
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <Typography variant="h5">Filters</Typography>
      {Object.entries(filters).map((f) => {
        return (
          <div key={f[0]} className={classes.filter}>
            <Typography variant="body1">{f[0]}</Typography>
            <ManageIcon
              data-filter={f[0]}
              aria-controls="simple-menu"
              aria-haspopup="true"
              onClick={handleClick(f[0])}
            />
            <Filter value={f[1].value} type={f[1].type} filter={f[0]} />
          </div>
        );
      })}
      {currentFilter !== "" ? (
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          {filters[currentFilter].available.map((a) => (
            <MenuItem key={a} onClick={handleClose} value={a}>
              {a}
            </MenuItem>
          ))}
        </Menu>
      ) : null}

      <button type="button" onClick={() => console.log("filters", filters)}>
        Log filters
      </button>
      <button
        type="button"
        onClick={() => console.log("filterState", filterState)}
      >
        Log filterState
      </button>
    </div>
  );
}
