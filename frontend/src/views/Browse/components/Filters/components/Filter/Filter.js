import React from "react";
import Input from "@material-ui/core/Input";
import Switch from "@material-ui/core/Switch";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Slider from "@material-ui/core/Slider";
import { startCase, snakeCase } from "lodash";

import {
  filters,
  cups as cupsVar,
} from "../../../../../../reactiveVariables/index";

export default function Filter(props) {
  const handleChange = (e, newValue) => {
    const tmp = filters();
    if (props.type === "string") {
      filters({
        ...tmp,
        ...{
          [props.filter]: {
            ...tmp[props.filter],
            value: e.target.value,
          },
        },
      });
    } else if (props.type === "boolean") {
      filters({
        ...tmp,
        ...{
          [props.filter]: {
            ...tmp[props.filter],
            value: e.target.checked,
          },
        },
      });
    } else if (props.type === "select") {
      filters({
        ...tmp,
        ...{
          [props.filter]: {
            ...tmp[props.filter],
            value: e.target.value,
          },
        },
      });
    } else if (props.type === "range") {
      filters({
        ...tmp,
        ...{
          [props.filter]: {
            ...tmp[props.filter],
            value: newValue,
          },
        },
      });
    }
  };

  if (props.type === "string")
    return (
      <Input
        placeholder={props.value || "Placeholder"}
        inputProps={{ "aria-label": "description" }}
        value={props.value}
        onChange={handleChange}
      />
    );
  if (props.type === "boolean")
    return (
      <Switch
        checked={props.value}
        onChange={handleChange}
        name={props.filter}
      />
    );
  if (props.type === "select") {
    const options = cupsVar()
      .reduce((acc, curr) => {
        if (!acc.includes(curr[props.filter])) acc.push(curr[props.filter]);
        return acc;
      }, [])
      .map(startCase);
    return (
      <Select value={props.value} onChange={handleChange}>
        {options.map((o) => (
          <MenuItem key={o} value={snakeCase(o)}>
            {o}
          </MenuItem>
        ))}
      </Select>
    );
  }
  if (props.type === "range") {
    const values = cupsVar().reduce((acc, curr) => {
      acc.push(curr[props.filter]);
      return acc;
    }, []);
    const max = Math.max(...values);
    const marks = [
      { value: 0, label: "0" },
      { value: max, label: `${max}mg` },
    ];
    function valuetext(value) {
      return `${value}mg`;
    }
    return (
      <Slider
        value={props.value}
        getAriaValueText={valuetext}
        step={10}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
        marks={marks}
        max={max}
        onChange={handleChange}
      />
    );
  }
  return <div></div>;
}
