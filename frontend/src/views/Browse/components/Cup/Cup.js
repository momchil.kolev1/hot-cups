import React from "react";
import { makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
// import { ReactComponent as Coffee } from "../../../../icons/coffee.svg";
// import { ReactComponent as Tea } from "../../../../icons/tea.svg";
import coffee from "../../../../icons/coffee.svg";
import tea from "../../../../icons/tea.svg";

const useStyles = makeStyles({
  img: {
    width: "100%",
  },
});

export default function Cup(props) {
  const classes = useStyles();
  const { name, type, caffeine } = props;
  return (
    <div>
      {/* {type === "coffee" ? <Coffee /> : <Tea />} */}
      <img className={classes.img} src={type === "coffee" ? coffee : tea} />
      <Typography variant="body1">{name}</Typography>
      <Typography variant="body2">{caffeine}</Typography>
    </div>
  );
}
