import React from "react";
import { makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    height: 80,
    padding: "0 50px",
    display: "flex",
    alignItems: "center",
  },
});

export default function Navbar() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography variant="h2">Hot Cups</Typography>
    </div>
  );
}
