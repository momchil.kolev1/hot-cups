import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core";
import { ApolloProvider, gql } from "@apollo/client";
import { client } from "./client";

import "./index.css";
import App from "./App";
import theme from "./theme";
import reportWebVitals from "./reportWebVitals";

// client
//   .query({
//     query: gql`
//       query {
//         cups {
//           # id
//           name
//           type
//           caffeine
//         }
//       }
//     `,
//   })
//   .then((result) => console.log(result));

ReactDOM.render(
  // <React.StrictMode>
  <Router>
    <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </ApolloProvider>
  </Router>,
  // </React.StrictMode>
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
