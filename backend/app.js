const { ApolloServer, gql } = require("apollo-server");

const typeDefs = gql`
  type Cup {
    id: ID
    name: String
    type: String
    caffeine: Int
  }
  type Query {
    cups: [Cup]
  }
`;

const cups = [
  {
    id: 0,
    name: "Earl Grey",
    type: "black_tea",
    caffeine: 45,
  },
  {
    id: 1,
    name: "Black Tea",
    type: "black_tea",
    caffeine: 50,
  },
  {
    id: 2,
    name: "Green Tea",
    type: "green_tea",
    caffeine: 40,
  },
  {
    id: 3,
    name: "Herbal Tea",
    type: "herbal_tea",
    caffeine: 0,
  },
  {
    id: 4,
    name: "Oolong Tea",
    type: "oolong_tea",
    caffeine: 40,
  },
  {
    id: 5,
    name: "Rooibos Tea",
    type: "herbal_tea",
    caffeine: 0,
  },
  {
    id: 6,
    name: "Espresso",
    type: "coffee",
    caffeine: 145,
  },
  {
    id: 7,
    name: "Instant Coffee",
    type: "coffee",
    caffeine: 80,
  },
  {
    id: 8,
    name: "Decaf Coffee",
    type: "coffee",
    caffeine: 2,
  },
];

const resolvers = {
  Query: {
    cups: () => cups,
  },
};

const server = new ApolloServer({ typeDefs, resolvers });

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
